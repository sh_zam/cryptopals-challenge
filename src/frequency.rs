use std::collections::HashMap;


pub fn frequency_lookup(c: char) -> f32 {
	// copied frequencies from:
	// https://laconicwolf.com/2018/06/05/cryptopals-challenge-4-detect-single-character-xor-encryption/
	let mut frequencies = HashMap::new();
	frequencies.insert('a', 0.08167f32);
	frequencies.insert('b', 0.01492f32);
	frequencies.insert('c', 0.02782f32);
	frequencies.insert('d', 0.04253f32);
	frequencies.insert('e', 0.12702f32);
	frequencies.insert('f', 0.02228f32);
	frequencies.insert('g', 0.02015f32);
	frequencies.insert('h', 0.06094f32);
	frequencies.insert('i', 0.06094f32);
	frequencies.insert('j', 0.00153f32);
	frequencies.insert('k', 0.00772f32);
	frequencies.insert('l', 0.04025f32);
	frequencies.insert('m', 0.02406f32);
	frequencies.insert('n', 0.06749f32);
	frequencies.insert('o', 0.07507f32);
	frequencies.insert('p', 0.01929f32);
	frequencies.insert('q', 0.00095f32);
	frequencies.insert('r', 0.05987f32);
	frequencies.insert('s', 0.06327f32);
	frequencies.insert('t', 0.09056f32);
	frequencies.insert('u', 0.02758f32);
	frequencies.insert('v', 0.00978f32);
	frequencies.insert('w', 0.02360f32);
	frequencies.insert('x', 0.00150f32);
	frequencies.insert('y', 0.01974f32);
	frequencies.insert('z', 0.00074f32);
	frequencies.insert(' ', 0.13000f32);

	return *frequencies.get(&c).unwrap_or(&-1.0);
}


pub fn get_frequency(s: &str) -> f32 {
	let mut sum: f32 = 0.0;

	for i in s.chars() {
		sum += frequency_lookup(i);
	}
	sum
}