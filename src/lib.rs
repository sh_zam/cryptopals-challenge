use hex;
use base64;

pub mod frequency;


pub fn hex_to_base64(s: &str) -> String {
	base64::encode(&hex::decode(s).unwrap())
}


pub fn xor(this: &[u8], that: &[u8]) -> Vec<u8> {
	let mut xored = Vec::new();
	for i in 0..this.len() {
		xored.push(this[i] ^ that[i]);
	}
	xored
}


pub fn string_xor(s: &[u8], c: u8) -> String {
	let mut xored = String::new();
	for i in s {
		xored.push((i ^ c) as char);
	}
	xored
}


pub fn repeating_key(plain_txt: &[u8], key: &[u8]) -> Vec<u8> {
	let mut key_it = 0;
	let mut xored = Vec::new();

	for i in plain_txt {
		xored.push(i ^ key[key_it]);
		key_it = (key_it + 1) % key.len();
	}
	xored
}


pub fn u8_to_string(s: &[u8]) -> String {
	let mut converted = String::new();
	for i in s {
		converted.push(*i as char);
	}
	converted
}


#[allow(dead_code)]
fn pad(s: &[u8], value: usize) -> Vec<u8> {
	let mut s = s.to_owned();
	for _ in 1..=s.len() % value {
		s.push(0);
	}
	s
}

fn pad_bin(i: &u8) -> String {
	let pad_data = "00000000";
	let mut s: String = format!("{:b}", i);	
	s.insert_str(0, &pad_data[0..(8 - s.len())]);
	s
} 


pub fn hamming_distance(this: &[u8], that: &[u8]) -> u32 {
	let this: String = this.into_iter().map(|i| pad_bin(i)).collect::<String>();
	let that: String = that.into_iter().map(|i| pad_bin(i)).collect::<String>();

	let mut distance = 0;
	for i in this.chars().zip(that.chars()) {
		if (i.0 as u8) ^ (i.1 as u8) != 0 {
			distance += 1;
		}
	}
	distance
}

pub fn single_key_xor(cipher: &[u8]) -> (f32, u8, String) {
	let mut maximum = std::f32::MIN;
	let mut result_key = 0;
	let mut result_string = String::new();

	for i in 0x00..0x7f {
		let tmp = string_xor(cipher, i);
		if maximum < frequency::get_frequency(&tmp) {
			maximum = frequency::get_frequency(&tmp);
			result_key = i;
			result_string = tmp;
		}
	}
	(maximum, result_key, result_string)
}

pub fn transpose_blocks(data: &[u8], blocksize: usize) -> Vec<Vec<u8>> {
	let mut transposed: Vec<Vec<u8>> = Vec::new();
	for i in 0..data.len() {
		let parent_index = i % blocksize;
		if None == transposed.get(parent_index) {
			transposed.push(vec![]);
		}
		transposed[parent_index].push(data[i]);
	}
	transposed
}


pub fn blockify(s: &[u8], blocksize: usize) -> Vec<&[u8]> {
	let mut blocks = Vec::new();
	for i in blocksize..s.len() {
		blocks.push(&s[i-blocksize..i]);
	}
	blocks
}


#[cfg(test)]
mod tests {

	use super::*;
	use std::fs::File;
	use std::io::{BufReader, BufRead};
	use crypto::aes;
	use crypto::blockmodes;


	#[test]
	fn challenge_1() {
		assert_eq!(hex_to_base64("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"),
		"SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t");
	}

	#[test]
	fn challenge_2() {
		assert_eq!(
		        xor(b"1c0111001f010100061a024b53535009181c", b"686974207468652062756c6c277320657965"), 
		        hex::decode("746865206b696420646f6e277420706c6179").unwrap());
	}

	#[test]
	fn challenge_3() {
		let cipher = hex::decode("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736").unwrap();
		let result = single_key_xor(&cipher).2;
		println!("{}", result);
	}

	#[ignore]
	#[test]
	fn challenge_4() {
		let mut maximum = 0f32;
		let mut result = 0;
		let mut result_string = String::new();

		let file = File::open("challenge4.txt").unwrap();
		
		for line in BufReader::new(file).lines() {
			let line = line.unwrap();
			let data = single_key_xor(&hex::decode(&line).unwrap());
			if maximum < data.0 {
				maximum = data.0;
				result = data.1;
				result_string = data.2;
			}
		}
		println!("key: {}, msg: {}", result, result_string);
	}

	#[test]
	fn challenge_5() {
		assert_eq!(repeating_key(b"Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal", b"ICE"), 
		hex::decode("0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f").unwrap());
	}

	#[ignore]
	#[test]
	fn challenge_6() {
		let data = std::fs::read_to_string("challenge6.txt").expect("Couldn't write file to string");
		let data = base64::decode(&data.replace("\n", "")).expect("Couldn't decrypt base64");

		let possible_keysize = get_possible_keys(&data);

		let mut max_sum = std::f32::MIN;
		let mut key = Vec::new();
		for i in possible_keysize.iter() {
			let mut sum = 0f32;
			let mut v = Vec::new();

			for block in transpose_blocks(&data, *i) {
				let data = single_key_xor(&block);
				v.push(data.1);
				sum += data.0;
			}
			if sum > max_sum {
				max_sum = sum;
				key = v;
			}
		}
		let key_string = u8_to_string(&key);
		println!("key = {}", key_string);
		println!("{}", u8_to_string(&repeating_key(&data, &key)));
	}

	#[allow(dead_code)]
	fn get_possible_keys(data: &[u8]) -> Vec<usize> {
		let mut keys = vec![];
		
		let mut min_avg = std::u32::MAX;
		for keysize in 2..=40 {
			let mut average = 0;
			let mut i = keysize;
			while i < data.len() {
				let d = hamming_distance(
					      data.get(i - keysize..keysize).unwrap_or(data.get(i..).unwrap()),
				              // if the keysize * 2 is greater then line length,
					      // then just get last left over bytes
				              data.get(keysize..keysize * 2).unwrap_or(data.get(keysize..).unwrap())
				        );

				i += keysize;
				average += d;
			}
			average /= data.len() as u32;
			// if another minimum is found then remove current min elements
			if average < min_avg {
				min_avg = average;
				keys.clear();
			} 
			if average == min_avg {
				keys.push(keysize);
			}
		}
		keys	
	}

	#[test]
	fn challenge_7() {
		let data = std::fs::read_to_string("challenge7.txt").expect("Couldn't write file to string");
		let data = base64::decode(&data.replace("\n", "")).expect("Couldn't decrypt base64");

		let iv = b"YELLOW SUBMARINE";
		let pad_proc = blockmodes::NoPadding;
		let mut decryptor = aes::ecb_decryptor(aes::KeySize::KeySize128, iv, pad_proc);

		let mut write_buffer = vec![0u8; data.len()];

		let mut reader = crypto::buffer::RefReadBuffer::new(&data);
		let mut writer = crypto::buffer::RefWriteBuffer::new(&mut write_buffer);
		decryptor.decrypt(&mut reader, &mut writer, false).unwrap();
		println!("{}", u8_to_string(&write_buffer));
	}


	#[test]
	fn challenge_8() {

		let file = File::open("challenge8.txt").unwrap();
		for line in BufReader::new(file).lines() {
			let line = line.unwrap();

			let data = hex::decode(&line).unwrap();
			let blocks = blockify(&data, 16);
			for i in 0..blocks.len() {
				for j in i+1..blocks.len() {
					let xored = xor(blocks[i], blocks[j]);
					if xored == [0u8; 16] {
						println!("{}", line);
						return;
					}
				}
			}
		}
		
	}
}
